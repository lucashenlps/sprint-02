# CONCEITOS

Definition of Ready (DoR) -> Basicamente o que precisa estar pronto antes que o desenvolvimento das outras funções a serem implementadas nessa User Story comecem. Por exemplo, numa US onde o objetivo principal é realizar autenticação em um site, um DoR seria função de cadastro de usuário, pois sem usuário cadastrado, não é possível fazer login na plataforma.

Definition of Done (DoD) -> Tudo o que deve ser feito para que o produto final seja considerado como completo. Por exemplo, antes de mostrarmos para o cliente, a equipe de testadores deve automatizar os scripts necessários para verificar a qualidade e funcionalidade do produto.

Critério de Aceitação -> Como as funcionalidades devem funcionar para que o produto execute corretamente. Num exemplo onde o cliente está desenvolvendo um aplicativo de um banco, pode ser dada ao dizer durante a reunião "não deve ser possível sacar quantidade de dinheiro maior do que o saldo atual".

# EXEMPLOS

**Definition of Ready**

_◻️ Histórias de Usuário devem ter sido escritas com o padrão de escrita INVEST;_

_◻️ Histórias de Usuário devem possuir ao menos um critério de aceite;_

_◻️ Histórias de Usuário devem possuir wireframe de baixa fidelidade._

**Definition of Done**

_◻️ Funcionalidades devem ter sido testadas;_

_◻️ Testes unitários devem ter sido criados;_

_◻️ Funcionalidades devem ter atendido a todos os critérios de aceite;_

_◻️ Todas as funcionalidades devem ter sido testadas no Chrome, IE e Firefox;_

_◻️ O código deve ter sido revisado por outro desenvolvedor._