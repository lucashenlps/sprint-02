# Conceitos HTTP e APIs REST

# O que é uma API

Sigla de 'Interface de Programação de Aplicações', são serviços de integração de sistemas. 

Possibilitam benefícios como a segurança dos dados, facilidade no intercâmbio entre informações com diferentes linguagens de programação e a
monetização de acessos.



# HTTP

Hyper Text Transfer Protocol, é um protocolo que obtém recursos, como por exemplo documentos HTML. Servem também para intermediar a comunicação com APIs.

Requests: São criadas pelo cliente (acesso a um Website, chamada para uma API)

Responses: Retorno do servidor, acompanhadas de status code e informações solicitadas (json)

HTTP e HTTPS: HTTP é um protocolo baseado em texto, HTTPS garante a segurança das informações através de chaves

# Famílias status codes

1XX -> Informativa (utilizado durante as negociações entre cliente e servidor)

2XX -> Sucesso (requisição do cliente foi aceita com sucesso)

3XX -> Redirecionamento (necessário que o cliente tome alguma ação adicional para completar sua requisição)

4XX -> Erro de cliente (algo está errado com a requisição do cliente)

5XX -> Erros de servidor (algo está errado no servidor)

# APIs: REST x RESTful

Rest (Representational State Transfer): Estilo de arquitetura que define padrões que ajudam na comunicação entre sistemas através da Web, permite que o cliente e o servidor sejam implementados independentemente, sem a necessidade que um tenha conhecimento da implementação do outro.

Usa o protocolo HTTP e é baseado em contrato

RESTful: A diferença é que a API implementada precisa estar de acordo com todas as regras e restrições definidas para a construção de APIs REST.
Possui os mesmos princípios de REST.

# Atividade Complementar 2:

Pesquise sobre o significado dos diferentes status code de API REST. Entenda quais são as famílias e os códigos principais de cada uma. Não precisamos decorar todos, mas é importante conhecer os grupos e os mais frequentes. Quanto mais eloquente for a comunicação da API melhor!

## 1XX - Informativo

100 -> Continue (tudo até então está OK para o cliente prosseguir com a requisição ou ignorar caso já tenha terminado)

101 -> Switching protocols (indica que o protocolo do servidor está trocando)
    
103 -> Early Hints (Servidor preparando uma resposta)

## 2XX - Sucesso

200 -> OK (requisição bem sucedida)

201 -> Created (um novo recurso foi criado)

202 -> Accepted (requisição recebida, mas não pode atuar ainda)

203 -> Non-Authoritative Information (requisição realizada com sucesso porém o conteúdo foi alterado por um **proxy** da resposta com status 200 (OK) do servidor de origem)

204 -> No Content (solicitação bem sucedida e o cliente não precisa sair da página atual)

205 -> Reset Content (informa para que o cliente reconfigure a visualização do documento(por exemplo: limpar o conteúdo de um formulário) para redefinir um estado da tela ou atualizar a interface do usuário)

206 -> Partial Content (requisição bem sucedida e o corpo contém a sequência dos dados)

207 -> Multi-Status (pode haver uma mistura das respostas)

208 -> Already Reported (usado em um erro 207 para poupar armazenamento e evitar conflitos)

226 -> IM Used (servidor está retornando um delta para a requisição GET que foi recebida)

## 3XX - Redirecionamento

300 -> Multiple Choices (requisição tem mais de uma resposta possível)

301 -> Moved Permanently (recurso requisitado movido para a URL dada pela *Location* do cabeçalho)

302 -> Found (recurso requisitado movido TEMPORARIAMENTE para a URL dada pela *Location* do cabeçalho)

303 -> See Other (redirecionamento não faz link com o recurso requisitado em si, mas para outra página)

304 -> Not Modified (não é necessário retransmitir os recursos requisitados)

307 -> Temporary Redirect (recurso requisitado movido temporariamente a URL dada pela *Location* do cabeçalho)

308 -> Permanent Redirect (recurso requisitado permanentemente movido para a URL dada pela *Location* do cabeçalho)

## 4XX - Erro de cliente

400 -> Bad Request (servidor não pode ou não vai processar a requisição devido a algo interpretado como um erro de cliente)

401 -> Unauthorized (requisição do cliente incompleta devido à falta de credenciais de autenticação válidas para o recurso requisitado)

403 -> Forbidden (o servidor recebe e entende a requisição mas recusa autorização)

404 -> Not Found (servidor pôde encontrar o recurso requisitado)

406 -> Not Acceptable (servidor não foi capaz de criar uma resposta condizente a lista de valores aceitados definidos   )

409 -> Conflict (conflito de requisição no estado atual do recurso alvo)

429 -> Too Many Requests (usuário realizou muitas requisições em um determinado período de tempo)

## 5XX - Erro de servidor

500 -> Internal Server Error (foi encontrado uma condição inesperada pelo servidor que impede de terminar o que foi solicitado)

502 -> Bad Gateway (indica que o servidor, enquanto atuando como um gateway ou proxy, recebeu uma resposta inválida de um servidor acima)

503 -> Service Unavailable (servidor não está pronto para realizar a requisição)

504 -> Gateway Timeout (indica que o servidor, enquanto atuando como um gateway ou proxy, não recebeu uma resposta necessária para completar a requisição de um servidor acima)

505 -> HTTP Version Not Supported (versão HTTP usada na requisição não é suportada pelo servidor)


entender o projeto 

documentacao da api




