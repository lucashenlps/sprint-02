# O que é o DoR

Sigla corresponde a Definition of Ready, envolve o que deve estar pronto ou resolvido para que a fase de desenvolvimento comece

Caso esses critérios não estejam prontos, pode haver impedimentos e dificuldades na hora do desenvolvimento do produto, o que pode acarretar em retrabalho, atraso de prazos e estresse para todo o time

A definição do DoR é responsabilidade de todas as pessoas do time

## Quem é responsável por tornar um critério Ready?

A responsabilidade por tornar um critério ready é das lideranças do projeto, por exemplo, Team Lead, Scrum Master, Tech Lead e Product Owner. 

# O que é importante estar no DoR?

Para te ajudar a identificar o que é importante estar no DoR, faça as seguintes perguntas:

• O que o devTeam precisa ter para iniciar o desenvolvimento?

• O que precisa estar pronto para iniciar o desenvolvimento?

• Quais acessos, ferramentas, documentação e ambientes precisamos possuir para iniciar o desenvolvimento?

• Quais permissões e aprovações precisam ser realizadas para iniciar o desenvolvimento?

• Quais práticas e técnicas precisam ser feitas para iniciar o desenvolvimento?