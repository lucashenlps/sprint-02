# Plano de teste

## Estrutura e exemplo

1. Nome do projeto

Exemplo: Usabilidade do Site XYZ

2. Resumo

- Por que faremos o teste? - É um resumo do planejamento do teste

- Hipótese - crença inicial sobre alguma característica do software a ser testado, seja boa ou não

- Exemplo: Acredita-se que a usabilidade de uma interface que foi construída seguindo padrões conhecidos será boa. Se a interface do Site XYZ foi construída dessa maneira, a hipótese é ** A interface do Site XYZ tem uma boa usabilidade**

- Resultado do teste

- Teste de usabilidade - Exemplo: Deseja-se verificar se a usabilidade da interface do Site XYZ é boa e para isso serão aplicados teste com usuários pertencentes ao público-alvo

3. Pessoas envolvidas

- Quem são os testadores?

- Fulano de tal, fulano de tal2, fulano de tal 3

- Podem ser adicionados usuários recrutados para responder o teste

4. Funcionalidades ou Módulos a serem testados

- O que será especificamente testado? - Tem relação com a atividade de análise do teste

- Todos os membros do projeto deverão ter uma compreensão clara sobre o que será testado e o que não será

- No caso de testes de usabilidade ou de interface, indicar qual a interface que especificamente será testada

- Testes de unidade: colocar nome dos metodos

- Testes de integração: inserir nome do módulo de teste e das classes

5. Local dos testes

- Onde os testes serão feitos? - Tem relação com a atividade de implementação do teste

- Serão feitos em um laboratório ou tentarão emular o ambiente real de uso?

6. Recursos necessários (software, rede, sala, verba ...)

Aqui podemos relacionar tudo o que for necessário para a realização dos testes

ex: é necessária alguma infra-estrutura de comunicação? é preciso instalar algum software? é preciso gerar algum script de teste? é preciso gerar dados para os testes? etc...

7. Critérios usados

Tem relação com a atividade Modelagem do teste

• Quantidade de testes a serem feitos

• Como os testes serão avaliados

• Como os testes serão divididos entre as pessoas envolvidas

• Foi feita amostragem?

• Que tarefas serão feitas pelos usuários?

8. Riscos

Consiste no que deve ser feito caso algum imprevisto aconteça durante a execucação do teste, risco é um evento futuro que tem probabilidade de ocorrencia e que gera um potencial de perda.

exemplo: caso seja necessário uma conexão com a internet para a realização de um teste, o risco seria "falta de conexão com a internet via rede wifi"

9. Como os resultados do teste serão divulgados

Como os resultados finais do teste serão fornecidos após o término dos ciclos de testes

• Será gerado um outro documento?

• Relatório dos resultados dos testes

• Relatório de defeitos

• Quais serão as métricas usadas

10. Cronograma

é uma ferramenta comum no gerenciamento de projetos, ele determina quando as atividades a serem executadas em um projeto serão realizadas. Cada uma delas deve-se indicar a data inicial e a data final               